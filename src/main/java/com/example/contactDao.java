package com.example;

import java.util.ArrayList;
import java.util.List;

public class ContactDao implements IContactDao {

    List<String> contacts = new ArrayList<>();

    @Override
    public void addContact(String nom) {
        contacts.add(nom);
        
    }
    @Override
    public boolean isContactExist(String nom) {
        return contacts.stream().anyMatch(nom::equalsIgnoreCase);
    }

    @Override
    public void delete(String nom) {
        contacts.remove(nom);
    }
}
